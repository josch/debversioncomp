.PHONY: all
all: versions.rand versions.sorted testdpkg testapt testdose
	set -e; for t in testapt.py testdpkg.pl testdpkg testapt testdose; do \
		printf "running $$t..." >&2; \
		./$$t < versions.rand | cmp versions.sorted - && echo OK >&2; \
	done

versions.rand: versions.sorted
	# create a randomized version list using a reproducible seed value
	{ openssl enc -aes-256-ctr -pass pass:1612543740 -nosalt </dev/zero 2>/dev/null 4<&- | { \
	  tr ' ' '\n' <versions.sorted 3<&- | sort --random-source=/proc/self/fd/3 --random-sort; \
	} 3<&0 <&4 4<&-; } 4<&0 > versions.rand

testdpkg: testdpkg.c
	gcc testdpkg.c -ldpkg -o testdpkg

testapt: testapt.cc
	g++ testapt.cc -lapt-pkg -o testapt

testdose:
	ocamlfind ocamlc -package dose3.versioning -linkpkg testdose.ml -o testdose

versions.sorted:
	wget https://metasnap.debian.net/by-package/debian.sqlite3
	wget https://metasnap.debian.net/by-package/debian-backports.sqlite3
	wget https://metasnap.debian.net/by-package/debian-debug.sqlite3
	wget https://metasnap.debian.net/by-package/debian-ports.sqlite3
	wget https://metasnap.debian.net/by-package/debian-security.sqlite3
	wget https://metasnap.debian.net/by-package/debian-volatile.sqlite3
	{ sqlite3 debian.sqlite3 "select name from vers"; \
	  sqlite3 debian-backports.sqlite3 "select name from vers"; \
	  sqlite3 debian-debug.sqlite3 "select name from vers"; \
	  sqlite3 debian-ports.sqlite3 "select name from vers"; \
	  sqlite3 debian-security.sqlite3 "select name from vers"; \
	  sqlite3 debian-volatile.sqlite3 "select name from vers"; \
	} | sort -u | grep '^[0-9]' | ./testapt.py > versions.sorted
	rm debian.sqlite3 debian-backports.sqlite3 debian-debug.sqlite3 debian-ports.sqlite3 debian-security.sqlite3 debian-volatile.sqlite3

.PHONY: clean
clean:
	rm -f testapt testdose testdose.cmi testdose.cmo testdpkg versions.rand
