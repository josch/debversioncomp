Comparing Debian versions correctly is hard. This project allows comparing
existing implementation by sorting a text file containing all Debian versions
that have been used since 2005.

To run it, make sure you have the following packages installed:

    $ sudo apt install build-essenital libdpkg-dev libapt-pkg-dev libextlib-ocaml-dev ocaml-findlib ocaml libdose3-ocaml-dev

Then execute:

    $ make

The following parsers are currently tested:

 - dpkg (C and Perl)
 - apt (C++ and Python)
 - dose3 (OCaml)

Additionally, the following parsers could be added:

 - libben-ocaml (in lib/benl_base.ml and lib/benl_dpkg.c)

The following do not use their own parser:

 - python-debian uses python-apt
 - libghc-dpkg-dev uses libdpkg-dev
 - ruby-debian uses libapt-pkg-dev
 - dpkg-dev-el uses `dpkg --compare-versions`

Each of the scripts parses a list of versions coming from standard input, one
per line, and writes them in sorted ascending order to standard output, one per
line. Versions that compare as equal are written on the same line,
lexicographically sorted and separated by space. The ground truth output is
stored in the file `versions.sorted`.
