open ExtLib

let main () =
  let versions = List.sort ~cmp:(fun a b ->
      let ret = Dose_versioning.Debian.compare a b in
      if ret == 0 then compare a b
      else ret
    ) (Std.input_list stdin) in
  print_string (List.hd versions);
  ignore (List.fold_left (fun acc e ->
          if Dose_versioning.Debian.compare acc e == 0 then print_char ' '
          else print_char '\n'; print_string e; e
    ) (List.hd versions) (List.tl versions));
  print_char '\n';
;;

main ();;
