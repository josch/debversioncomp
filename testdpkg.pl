#!/usr/bin/perl

use strict;
use warnings;

use Dpkg::Version;

my @versions = ();
while ( my $line = <> ) {
    chomp $line;
    my $ver = Dpkg::Version->new( $line, check => 1 );
    die "not a valid version: $line" if !defined $ver;
    push @versions, [ $ver, $line ];
}

sub myvercomp {

    # Dpkg::Version overloads <=>
    my $ret = $a->[0] <=> $b->[0];
    if ( $ret == 0 ) {

        # fall back to string comparison
        return $a->[1] cmp $b->[1];
    }
    return $ret;
}
@versions = sort myvercomp @versions;

my $last = shift @versions;
print "$last->[1]";
foreach my $v (@versions) {
    if ( ( $last->[0] <=> $v->[0] ) == 0 ) {
        print " ";
    }
    else {
        print "\n";
    }
    $last = $v;
    print "$v->[1]";
}
print "\n";
