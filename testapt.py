#!/usr/bin/env python3

import sys
from functools import cmp_to_key
import apt_pkg

apt_pkg.init_system()


def myvercomp(a, b):
    ret = apt_pkg.version_compare(a, b)
    if ret == 0:
        return 1 if a > b else -1
    return ret


l = [line.rstrip("\n") for line in sys.stdin]
l = sorted(l, key=cmp_to_key(myvercomp))

last = l.pop(0)
print(last, end="")
for e in l:
    if apt_pkg.version_compare(last, e) == 0:
        print(" ", end="")
    else:
        print()
    last = e
    print(e, end="")
print()
