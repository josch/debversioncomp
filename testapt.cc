#include <apt-pkg/debversion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmpver(const void *a, const void *b) {
	int ret = debVS.CmpVersion(*(const char **) a, *(const char **) b);
	if (ret == 0) {
		return strcmp(*(const char **) a, *(const char **) b);
	}
	return ret;
}


int main()
{
    char *line = NULL;
    size_t len = 0;
    size_t read;
	char **versions = NULL;
	size_t num_versions = 0;
    while ((read = getline(&line, &len, stdin)) != -1) {
		num_versions += 1;
		versions = (char **)realloc(versions, sizeof(char *)*num_versions);
		if (versions == NULL) {
			perror("malloc failed");
			exit(EXIT_FAILURE);
		}
		if (line[read-1] == '\n') {
			line[read-1] = '\0';
		}
		versions[num_versions-1] = strdup(line);
	}
	qsort(versions, num_versions, sizeof(char *), cmpver);
	printf(versions[0]);
	for (int i = 1; i < num_versions; i++) {
		if (debVS.CmpVersion(versions[i-1], versions[i]) == 0) {
			printf(" ");
		} else {
			printf("\n");
		}
		printf(versions[i]);
	}
	printf("\n");
    exit(EXIT_SUCCESS);
}
